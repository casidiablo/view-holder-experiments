package com.view.holder.codegen;

import com.squareup.javawriter.JavaWriter;
import com.view.holder.annotations.BindWith;
import com.view.holder.annotations.Layout;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import static java.lang.String.format;

public class ViewHolderAnnotationProcessor extends AbstractProcessor {

  @Override
  public boolean process(Set<? extends TypeElement> typeElements, RoundEnvironment roundEnv) {
    if (typeElements.isEmpty()) {
      return true;
    }

    for (TypeElement typeElement : typeElements) {
      Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(typeElement);
      for (Element annotatedElement : annotatedElements) {
        try {
          writeSomeJava(annotatedElement);
        } catch (IOException e) {
          Messager messager = processingEnv.getMessager();
          messager.printMessage(Diagnostic.Kind.ERROR, e.getMessage());
        }
      }
    }
    return false;
  }

  @Override
  public Set<String> getSupportedAnnotationTypes() {
    HashSet<String> annotations = new HashSet<String>();
    annotations.add("com.view.holder.annotations.Layout");
    return annotations;
  }

  @Override
  public SourceVersion getSupportedSourceVersion() {
    return SourceVersion.RELEASE_6;
  }

  private void writeSomeJava(Element annotatedElement) throws IOException {
    Name genericTypeName = annotatedElement.getSimpleName();

    String packageName = "com.view.holder.gen";
    String className = "GenericAdapter$" + genericTypeName;
    String fullClassName = packageName + "." + className;

    Filer filer = processingEnv.getFiler();
    JavaFileObject sourceFile = filer.createSourceFile(fullClassName);
    Writer out = sourceFile.openWriter();

    JavaWriter javaWriter = new JavaWriter(out);
    javaWriter.emitSingleLineComment("This is a generated file, don't touch!");
    javaWriter.emitPackage(packageName);

    javaWriter.emitImports("com.view.holder.GenericAdapter");
    javaWriter.emitImports("android.content.Context");
    javaWriter.emitImports("android.view.View");
    javaWriter.emitImports("android.widget.ImageView");
    javaWriter.emitImports("android.widget.TextView");

    javaWriter.emitImports(String.valueOf(annotatedElement));
    javaWriter.emitEmptyLine();

    String extendsType = format("GenericAdapter<%s>", genericTypeName);
    javaWriter.beginType(className, "class", EnumSet.of(Modifier.PUBLIC), extendsType);

    List<String> params = Arrays.asList(genericTypeName + " item,", "View view");

    javaWriter.beginConstructor(EnumSet.of(Modifier.PUBLIC), "Context", "context");
    javaWriter.emitStatement("super(context)");
    javaWriter.endConstructor();

    javaWriter.emitAnnotation(Override.class);
    javaWriter.beginMethod("View", "getView", EnumSet.of(Modifier.PUBLIC), params, null);

    javaWriter.emitStatement("ViewHolder holder");

    javaWriter.emitSingleLineComment("PHASE 1: view inflation");
    javaWriter.beginControlFlow("if (view == null)");

    javaWriter.emitStatement("holder = new ViewHolder()");

    // 1. get the layout id
    Layout annotation = annotatedElement.getAnnotation(Layout.class);
    javaWriter.emitStatement("view = getInflater().inflate(%s, null)", annotation.value());

    // 2. inflate the views
    List<? extends Element> enclosedElements = annotatedElement.getEnclosedElements();
    for (Element field : enclosedElements) {
      BindWith bindWith = field.getAnnotation(BindWith.class);
      if (field.getKind() == ElementKind.FIELD && bindWith != null) {
        int viewId = bindWith.value();
        javaWriter.emitStatement("holder.%s = view.findViewById(%s)", field.getSimpleName(), viewId);
      }
    }

    javaWriter.emitStatement("view.setTag(holder)");

    javaWriter.nextControlFlow("else");

    javaWriter.emitStatement("holder = (ViewHolder) view.getTag()");

    javaWriter.endControlFlow();

    javaWriter.emitEmptyLine();

    // DATA binding!!
    javaWriter.emitSingleLineComment("PHASE 2: data binding");

    for (Element field : enclosedElements) {
      BindWith bindWith = field.getAnnotation(BindWith.class);
      if (field.getKind() != ElementKind.FIELD || bindWith == null) {
        continue;
      }

      Name fieldName = field.getSimpleName();

      javaWriter.beginControlFlow(format("if (holder.%s instanceof TextView)", fieldName));
      javaWriter.emitStatement("((TextView) holder.%s).setText(item.%s)", fieldName, fieldName);

      javaWriter.nextControlFlow(format("else if (holder.%s instanceof ImageView)", fieldName));
      javaWriter.emitStatement("getImageUtils().setImage((ImageView) holder.%s, item.%s)", fieldName, fieldName);
      javaWriter.endControlFlow();

      javaWriter.emitEmptyLine();
    }

    javaWriter.emitStatement("return view");
    javaWriter.endMethod();

    javaWriter.emitEmptyLine();
    javaWriter.beginType("ViewHolder", "class", EnumSet.of(Modifier.PRIVATE, Modifier.STATIC));

    for (Element field : enclosedElements) {
      BindWith bindWith = field.getAnnotation(BindWith.class);
      if (field.getKind() == ElementKind.FIELD && bindWith != null) {
        javaWriter.emitField("View", String.valueOf(field.getSimpleName()));
      }
    }

    javaWriter.endType();

    javaWriter.endType();
    out.flush();
    out.close();
  }
}