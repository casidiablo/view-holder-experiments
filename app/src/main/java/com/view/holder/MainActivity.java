package com.view.holder;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.view.holder.annotations.Movie;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {
  private GenericAdapter<Movie> mAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_main);

    mAdapter = AdapterFactory.newAdapter(this, com.view.holder.annotations.Movie.class);
    final List<Movie> movies = getMovies();
    movies.addAll(getMovies());
    movies.addAll(getMovies());
    movies.addAll(getMovies());
    movies.addAll(getMovies());
    movies.addAll(getMovies());
    mAdapter.update(movies);

    ListView listView = (ListView) findViewById(R.id.list_id);
    listView.setAdapter(mAdapter);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    mAdapter.report();
  }


  private List<Movie> getMovies() {
    final ArrayList<Movie> movies = new ArrayList<Movie>();

    final Movie oz = new Movie();
    oz.thumbnail = "http://content.internetvideoarchive.com/content/photos/6432/27016911_.jpg";
    oz.title = "The Wizard of Oz";
    oz.description = "An absolute masterpiece whose groundbreaking visuals and deft storytelling are still every bit as resonant";
    movies.add(oz);

    final Movie kane = new Movie();
    kane.thumbnail = "http://content.internetvideoarchive.com/content/photos/7469/98446189.jpg";
    kane.title = "Citizen Kane";
    kane.description = "Orson Welles's epic tale of a publishing tycoon's rise and fall is entertaining, poignant, and inventive in its storytelling.";
    movies.add(kane);

    final Movie godFather = new Movie();
    godFather.thumbnail = "http://content.internetvideoarchive.com/content/photos/014/000062_46.jpg";
    godFather.title = "The Godfather";
    godFather.description = "One of Hollywood's greatest critical and commercial successes";
    movies.add(godFather);

    final Movie metropolis = new Movie();
    metropolis.thumbnail = "http://content.internetvideoarchive.com/content/photos/7370/42626.jpg";
    metropolis.title = "Metropolis";
    metropolis.description = "A visually awe-inspiring science fiction classic from the silent era.";
    movies.add(metropolis);

    final Movie sing = new Movie();
    sing.thumbnail = "http://content.internetvideoarchive.com/content/photos/7986/65125_005.jpg";
    sing.title = "Signing In The Rain";
    sing.description = "Clever, incisive, and funny, Singin' in the Rain is a masterpiece of the classical Hollywood musical.";
    movies.add(sing);

    final Movie taxi = new Movie();
    taxi.thumbnail = "http://content.internetvideoarchive.com/content/photos/8835/828032_060.jpg";
    taxi.title = "Taxi Driver";
    taxi.description = "A must-see film for movie lovers, this Martin Scorsese masterpiece is as hard-hitting as it is compelling, with Robert De Niro at his best.";
    movies.add(taxi);

    final Movie no = new Movie();
    no.thumbnail = "http://content.internetvideoarchive.com/content/photos/8081/416627_018.jpg";
    no.title = "Dr. No";
    no.description = "Featuring plenty of the humor, action, and escapist thrills the series would become known for, Dr. No kicks off the Bond franchise in style.";
    movies.add(no);

    final Movie lego = new Movie();
    lego.thumbnail = "http://content.internetvideoarchive.com/content/photos/7559/88845_067.jpg";
    lego.title = "Lego";
    lego.description = "Boasting beautiful animation, a charming voice cast, laugh-a-minute gags, and a surprisingly thoughtful story, The Lego Movie is colorful fun for all ages.";
    movies.add(lego);

    final Movie plane = new Movie();
    plane.thumbnail = "http://content.internetvideoarchive.com/content/photos/815/034245_5.jpg";
    plane.title = "Airplane";
    plane.description = "Though unabashedly juvenile and silly, Airplane! is nevertheless an uproarious spoof comedy full of quotable lines and slapstick gags that endure to this day.";
    movies.add(plane);

    return movies;
  }
}
