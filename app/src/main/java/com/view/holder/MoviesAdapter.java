package com.view.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MoviesAdapter extends GenericAdapter<Movie> {

  public MoviesAdapter(Context context) {
    super(context);
  }

  @Override
  public View getView(Movie item, View view) {
    ViewHolder holder;
    // PHASE 1: View inflation
    // Ingredients: layout id and views' ids
    if (view == null) {
      holder = new ViewHolder();

      view = getInflater().inflate(R.layout.moview_row, null);
      holder.thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
      holder.title = (TextView) view.findViewById(R.id.title);
      holder.description = (TextView) view.findViewById(R.id.description);

      view.setTag(holder);
    } else {
      holder = (ViewHolder) view.getTag();
    }

    // PHASE 2: data binding
    // Ingredients: data and views
    getImageUtils().setImage(holder.thumbnail, item.thumbnail);
    holder.title.setText(item.title);
    holder.description.setText(item.description);

    return view;
  }

  static class ViewHolder {
    ImageView thumbnail;
    TextView title;
    TextView description;
  }
}
