package com.view.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

import java.util.ArrayList;
import java.util.List;

public abstract class GenericAdapter<Item> extends BaseAdapter {

  private final List<Item> mItems = new ArrayList<Item>();
  private final LayoutInflater mInflater;
  private final Context mContext;
  private final ImageUtils mImageUtils;
  private final Monitor mGetView;

  public GenericAdapter(Context context) {
    mContext = context;
    mInflater = LayoutInflater.from(context);
    mImageUtils = ImageUtils.getInstance(getContext());
    mGetView = MonitorFactory.start("getView");
  }

  @Override
  public int getCount() {
    return mItems.size();
  }

  @Override
  public Item getItem(int i) {
    return mItems.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    mGetView.start();
    view = getView(getItem(i), view);
    mGetView.stop();
    return view;
  }

  protected LayoutInflater getInflater() {
    return mInflater;
  }

  protected Context getContext() {
    return mContext;
  }

  public ImageUtils getImageUtils() {
    return mImageUtils;
  }

  public void update(List<Item> items) {
    mItems.clear();
    mItems.addAll(items);
    notifyDataSetChanged();
  }

  public void report() {
    System.out.println("mGetView.getAvg() = " + mGetView.getAvg());
    System.out.println("mGetView.getHits() = " + mGetView.getHits());
    System.out.println("mGetView.getMax() = " + mGetView.getMax());
    System.out.println("mGetView.getMin() = " + mGetView.getMin());
  }

  protected abstract View getView(Item item, View view);
}
