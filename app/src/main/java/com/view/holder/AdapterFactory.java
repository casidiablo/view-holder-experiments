package com.view.holder;

import android.content.Context;

import com.view.holder.annotations.AnnotationsAdapter;

import java.lang.reflect.Constructor;

public class AdapterFactory {
  public static <T> GenericAdapter<T> newAdapter(Context context, Class<T> type) {
    try {
      String className = "com.view.holder.gen.GenericAdapter$" + type.getSimpleName();
      Class<? extends GenericAdapter> genericAdapterClass =
          Class.forName(className).asSubclass(GenericAdapter.class);
      Constructor<? extends GenericAdapter> declaredConstructor = genericAdapterClass.getDeclaredConstructor(Context.class);
      return declaredConstructor.newInstance(context);
    } catch (Exception e) {
      e.printStackTrace();

      return new AnnotationsAdapter<T>(context);
    }
  }
}
