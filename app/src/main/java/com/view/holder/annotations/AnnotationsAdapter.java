package com.view.holder.annotations;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.view.holder.GenericAdapter;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class AnnotationsAdapter<Item> extends GenericAdapter<Item> {
  public AnnotationsAdapter(Context context) {
    super(context);
  }

  @Override
  protected View getView(Item item, View view) {
    Class<?> itemClass = item.getClass();

    Map<Field, View> holder;
    // PHASE 1: View inflation
    // Ingredients: layout id and views' ids
    if (view == null) {
      holder = new HashMap<>();

      // 1. get layout id
      if (!itemClass.isAnnotationPresent(Layout.class)) {
        throw new RuntimeException("Class is not annotated with @Layout");
      }
      Layout layout = itemClass.getAnnotation(Layout.class);
      int resId = layout.value();

      view = getInflater().inflate(resId, null);

      // 2. inflate the views
      for (Field field : itemClass.getDeclaredFields()) {
        if (!field.isAnnotationPresent(BindWith.class)) {
          continue;
        }

        BindWith bindWith = field.getAnnotation(BindWith.class);
        int viewId = bindWith.value();

        View viewById = view.findViewById(viewId);
        holder.put(field, viewById);
      }

      view.setTag(holder);
    } else {
      //noinspection unchecked
      holder = (Map<Field, View>) view.getTag();
    }

    // PHASE 2: data binding
    // Ingredients: data and views
    for (Map.Entry<Field, View> fieldViewEntry : holder.entrySet()) {
      Field field = fieldViewEntry.getKey();
      View bindView = fieldViewEntry.getValue();

      Object value = null;
      try {
        value = field.get(item);
      } catch (IllegalAccessException ignore) {
      }

      if (bindView instanceof TextView) {
        TextView textView = (TextView) bindView;
        textView.setText(String.valueOf(value));
      }

      if (bindView instanceof ImageView) {
        ImageView imageView = (ImageView) bindView;
        getImageUtils().setImage(imageView, String.valueOf(value));
      }
    }

    return view;
  }
}
