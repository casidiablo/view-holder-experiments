package com.view.holder.annotations;

import com.view.holder.R;

@Layout(R.layout.moview_row)
public class Movie {
  @BindWith(R.id.thumbnail)
  public String thumbnail;

  @BindWith(R.id.title)
  public String title;

  @BindWith(R.id.description)
  public String description;
}
