package com.view.holder;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageUtils {
  private static ImageUtils ourInstance;

  public static ImageUtils getInstance(Context context) {
    if (ourInstance == null) {
      ourInstance = new ImageUtils(context);
    }
    return ourInstance;
  }

  private Picasso mPicasso;

  private ImageUtils(Context context) {
    mPicasso = new Picasso.Builder(context).build();
  }

  public void setImage(ImageView view, String url) {
    mPicasso.load(url).into(view);
  }
}
